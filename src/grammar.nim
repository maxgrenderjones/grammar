import nre
import options
import strutils except isAlpha, isLower, isUpper, isSpace
import tables
import os
import terminal
import unittest

export isSpaceAscii, isLowerAscii, isUpperAscii

const debugLex = false

template debug(enable: bool, text: string): void =
  when enable:
    echo(text)

type
  Parser[N, T] = proc(text: T, start: int, nodes: var seq[Node[N, T]]): int {.closure.}

  RuleObj[N, T] = object
    parser: Parser[N, T]
    kind: N

  Rule*[N, T] = ref RuleObj[N, T]

  NodeKind = enum
    nkTerminal,
    nkNonTerminal

  Node*[N, T] = ref object of RootObj
    case nodeKind*: NodeKind
      of nkNonTerminal:
        children*: seq[Node[N, T]]
      of nkTerminal:
        discard
    start*: int
    length*: int
    kind*: N
    source*: T

proc newRule[N, T](parser: Parser, kind: N): Rule[N, T] =
  new(result)
  result.parser = parser
  result.kind = kind

proc newRule[N, T](kind: N): Rule[N, T] =
  new(result)
  result.kind = kind

proc newNode[N, T](start: int, length: int, kind: N, text: T): Node[N, T] =
  result = Node[N, T](nodeKind: nkTerminal)
  result.start = start
  result.length = length
  result.kind = kind
  result.source = text

proc newNode[N, T](start: int, length: int, children: seq[Node[N, T]], kind: N, text: T): Node[N, T] =
  result = Node[N, T](nodeKind: nkNonTerminal)
  result.start = start
  result.length = length
  result.children = children
  result.kind = kind
  result.source = text

proc substr[T](text: T, first, last: int): T =
  text[first .. last]

func continuesWith[N, T](text: seq[Node[N, T]], subtext: seq[N], start: Natural): bool =
  let length = len(text)
  var pos = 0
  while pos < len(subtext):
    let textpos = start + pos
    if textpos == len(text):
      return false
    if text[textpos].kind != subtext[pos].kind:
      return false
    pos+=1
  return true

proc value*[N, T](node: Node[N, T]): string =
  ## Returns the text referred to by the node
  case node.nodeKind:
    of NodeKind.nkTerminal:
      when T is string:
        result = node.source[node.start ..< node.start + node.length]
      else:
        result = ""
        for n in node.source[node.start ..< node.start + node.length]:
          result &= n.value()
    of NodeKind.nkNonTerminal:
      result = ""
      for n in node.children:
        result &= n.value()

proc render*[N, T](nodes: seq[Node[N, T]]): string

proc render*[N, T](node: Node[N, T]): string =
  ## Returns the value of the node, prefixed by its kind
  case node.nodeKind:
    of NodeKind.nkTerminal:
      when T is string:
        result = "<" & $node.kind & ":" & node.value() & ">"
      else:
        result = "[" & $node.kind & ":"
        for n in node.source[node.start .. node.start + node.length]:
          result &= n.render()
        result &= "]"
    of NodeKind.nkNonTerminal:
      result = "{" & $node.kind & ":"
      result &= node.children.render()
      result &= "}"

proc render*[N, T](nodes: seq[Node[N, T]]): string =
  ## Returns a concatenation of the render of a sequence of nodes
  result = ""
  for node in nodes:
    result &= node.render()

proc parse*[N: enum, T: string|seq](rule: Rule[N, T], text: T, start = 0): seq[Node[N, T]] =
  ## Attempts to parse the suppled ``text`` using the given ``rule``, starting at position ``start``
  result = newSeq[Node[N, T]]()

  when T is string:
    let printabletext = text
  elif T is seq[Node]:
    let printabletext = text.render()
  debug(debugLex, "Parsing: " & $printabletext)

  let length = rule.parser(text, start, result)
  
  if length == -1:
    echo("Match failed: " & $printabletext)
    # result = nil # seq are non-nilable
  elif length == len(text):
    debug(debugLex, "Matched: " & $printabletext & " => " & $len(result) & " tokens: " & result.render())
  else:
    echo("Matched first " & $length & "/" & $len(text) & " symbols: " & $printabletext & " => " & $len(result) & " tokens: " & result.render())

proc literal*[N, T, P](pattern: P, kind: N): Rule[N, T] =
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    if start == len(text):
      return -1
    assert(len(text)>start, "Attempting to match at $#, string length is $# " % [$start, $len(text)])
    when P is string or P is seq[N]:  # FIXME: seq[N] needs to be matched to the kinds
      debug(debugLex, "Literal[" & $kind & "]: testing " & $pattern & " at " & $start & ": " & $text[start..start+len(pattern)-1])
      if text.continuesWith(pattern, start):
        let node = newNode(start, len(pattern), kind, text)
        nodes.add(node)
        debug(debugLex, "Literal: matched <" & $text[start ..< start+node.length] & ":" & $node.length & ">" )
        return node.length
    elif P is char:
      debug(debugLex, "Literal[" & $kind & "]: testing " & $pattern & " at " & $start & ": " & $text[start])
      if text[start] == pattern:
        let node = newNode(start, 1, kind, text)
        nodes.add(node)
        return 1
    else: # P is an individual symbol
      debug(debugLex, "Literal[" & $kind & "]: testing " & $pattern & " at " & $start & ": " & $text[start].kind)
      if text[start].kind == pattern:
        let node = newNode(start, 1, kind, text)
        nodes.add(node)
        return 1
    return -1
  result = newRule[N, T](parser, kind)

proc iliteral*[N](pattern: string, kind: N): Rule[N, string] =
  let parser = proc (text: string, start: int, nodes: var seq[Node[N, string]]): int =
    if start + len(pattern) > len(text):
      return -1
    elif text[start ..< start+len(pattern)].toLowerAscii == pattern.toLowerAscii:
      let node = newNode(start, len(pattern), kind, text)
      nodes.add(node)
      return node.length
    return -1
  result = newRule[N, string](parser, kind)

proc token[N, T](pattern: T, kind: N): Rule[N, T] =
  when T is not string:
     {.fatal: "Token is only supported for strings".}
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    debug(debugLex, "Token[" & $kind & "]: testing " & pattern & " at " & $start)
    if start == len(text):
      return -1
    assert(len(text)>start, "Attempting to match at $#, string length is $# " % [$start, $len(text)])
    let m = text.match(re(pattern), start)
    if m.isSome:
      let node = newNode(start, len(m.get.match), kind, text)
      nodes.add(node)
      result = node.length
      debug(debugLex, "Token: matched <" & text[start ..< start+node.length] & ":" & $node.length & ">" )
    else:
      result = -1
  result = newRule[N, T](parser, kind)

proc chartest[N, T, S](testfunc: proc(s: S): bool, kind: N): Rule[N, T] =
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    if start == len(text):
      return -1
    assert(len(text)>start, "Attempting to match at $#, string length is $# " % [$start, $len(text)])
    when T is string:
      if testfunc(text[start]):
        nodes.add(newNode(start, 1, kind, text))
        result = 1
      else:
        result = -1
    elif T is seq: # [Node[S]]:
      if testfunc(text[start].kind):
        nodes.add(newNode(start, 1, kind, text))
        result = 1
      else:
        result = -1
    else:
      {.fatal: "text must be either a string or a sequence of nodes" .}
  result = newRule[N, T](parser, kind)

proc any*[N: enum](chars: string, kind: N): Rule[N, string] =
  let test = proc(c: char): bool =
    debug(debugLex, "Any[" & $kind & "]: testing for " & chars.replace("\n", "\\n").replace("\r", "\\r"))
    result = c in chars
  result = chartest[N, string, char](test, kind)

proc any*[N: enum, T: string|seq, S: enum](symbols: set[S]|seq[S], kind: N): Rule[N, T] =
  let test = proc(s: S): bool =
    debug(debugLex, "Any[" & $kind & "]: testing for " & $symbols & ": " & $s)
    return s in symbols
  result = chartest[N, T, S](test, kind)

proc ignore*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Ignores the contents of the rule supplied as an argument
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    result = rule.parser(text, start, mynodes)
  result = newRule[N, T](parser, rule.kind)

proc eof*[N, T](kind: N): Rule[N, T] =
  ## Matches the end of the text
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    result = if start == len(text): 0 else: -1
  result = newRule[N, T](parser, kind)

proc combine*[N, T](rule: Rule[N, T], kind: N): Rule[N, T] =
  ## Creates a new rule that matches from the beginning to the end of the rule supplied as an argument
  ## 
  ## Warning: ignored sections of the text may be re-surfaced
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    result = rule.parser(text, start, mynodes)
    nodes.add(newNode(start, result, kind, text))
  result = newRule[N, T](parser, kind)

proc build*[N, T](rule: Rule[N, T], kind: N): Rule[N, T] =
  ## Creates a non-terminal rule whose children are the matches of the rule supplied as an argument
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    result = rule.parser(text, start, mynodes)
    if result>=0:
      let nonTerminal = newNode(start, result, mynodes, kind, text)
      nodes.add(nonTerminal)
  result = newRule[N, T](parser, kind)

proc printPosition(text: string, position: int): tuple[line: int, col: int] = 
  result.line = countLines(text[0..position]) + 1
  var linestart = position
  var lineend = position
  while linestart>0:
    if text[linestart] in NewLines:
      break
    linestart-=1
  while lineend < len(text):
    if text[lineend] in NewLines:
      break
    lineend+=1
  result.col = position-linestart
  echo text.substr(linestart, lineend-1)
  echo ' '.repeat(max(result.col, 0)) & '^'

proc printPosition[N, T](text: seq[Node[N, T]], position: int): tuple[line: int, col: int] =
  let node = text[position]
  return printPosition(node.source, node.start) 

proc fail*[N, T](message: string, kind: N): Rule[N, T] =
  ## Fails the parse, printing an indicator as to where the failure happened to stdout
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    let pos = printPosition(text, start)
    raise newException(ValueError, "Position: " & $start & " Line: " & $pos.line & ", Column: " & $pos.col & ": " & message)
  result = newRule[N, T](parser, kind)

proc printError[N, T](node: Node[N, T]): tuple[line: int, col: int] =
  let text = node.source
  let pos = node.start
  let length = node.length
  result.line = countLines(text[0..pos]) + 1
  var linestart = pos
  var lineend = pos + length
  while linestart>0:
    if text[linestart] in NewLines:
      break
    linestart-=1
  while lineend < len(text):
    if text[lineend] in NewLines:
      break
    lineend+=1
  result.col = pos-linestart
  if isatty(stdout):
    stdout.styledWriteLine(text[linestart..<pos], fgRed, text[pos..<pos+length], resetStyle, text[pos+length ..< lineend])
  else:
    return printPosition(text, pos)

proc error*[N, T](message: string, kind: N): Rule[N, T] =
  ## Fails the parse, assuming that the previous matched node is the one that caused the failure
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    let pos = if len(nodes)>0:
      printError(nodes[nodes.high])
    else:
      printPosition(text, start)

    raise newException(ValueError, "Position: " & $start & " Line: " & $pos.line & ", Column: " & $pos.col & ": " & message)
  result = newRule[N, T](parser, kind)

proc dump*[N, T](message: string, kind: N): Rule[N, T] =
  ## Prints a dump of the parse up until this point
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    let pos = if len(nodes)>0:
        printError(nodes[nodes.high])
      else:
        printPosition(text, start)
    echo("Dumping at position: " & $start & " Line: " & $pos.line & ", Column: " & $pos.col & ": " & message)
    if nodes.high>0:
      echo(render(nodes))
    return 0
  result = newRule[N, T](parser, kind)

proc warn*[N, T](message: string, kind: N): Rule[N, T] =
  ## Fails the parse, assuming that the previous matched node is the one that caused the failure
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    let pos = printError(nodes[nodes.high])
    echo("Warning: Position: " & $start & " Line: " & $pos.line & ", Column: " & $pos.col & ": " & message)
  result = newRule[N, T](parser, kind)

proc `+`*[N, T](left: Rule[N, T], right: Rule[N, T]): Rule[N, T] =
  ## Matches if `left` matches and then `right` matches
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    assert(not isNil(left.parser), "Left hand side parser is nil")
    let leftlength = left.parser(text, start, mynodes)
    if leftlength == -1:
      return leftlength
    assert(not isNil(right.parser), "Right hand side parser is nil at pos: " & $start)
    let rightlength = right.parser(text, start+leftlength, mynodes)
    if rightlength == -1:
      return rightlength
    result = leftlength + rightlength
    nodes.add(mynodes)
  result = newRule[N, T](parser, left.kind)

proc `/`*[N, T](left: Rule[N, T], right: Rule[N, T]): Rule[N, T] =
  ## Matches if `left` or `right` matches
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    assert(not isNil(left.parser), "Left hand side of / is not fully defined")
    let leftlength = left.parser(text, start, mynodes)
    if leftlength != -1:
      nodes.add(mynodes)
      return leftlength
    mynodes = newSeq[Node[N, T]]()
    assert(not isNil(right.parser), "Right hand side of / is not fully defined")
    let rightlength = right.parser(text, start, mynodes)
    if rightlength == -1:
      return rightlength
    nodes.add(mynodes)
    return rightlength
  result = newRule[N, T](parser, left.kind)

proc `?`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Matches `rule` if it matches, otherwise succeeds anyway
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    let success = rule.parser(text, start, nodes)
    return if success != -1: success else: 0
  result = newRule[N, T](parser, rule.kind)

proc `+`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Matches at least one repetition of `rule`
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var success = rule.parser(text, start, nodes)
    if success == -1:
      return success
    var total = 0
    while success != -1 and start+total < len(text):
      total += success
      success = rule.parser(text, start+total, nodes)
    return total
  result = newRule[N, T](parser, rule.kind)

proc `*`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Matches any number of repetitions of `rule` (including none)
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    let success = (+rule).parser(text, start, nodes)
    return if success != -1: success else: 0
  result = newRule[N, T](parser, rule.kind)

#Note: this consumes - for zero-width lookahead see !
proc `^`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Matches anything but `rule` (Note that this consumes - for zero-width lookahead see `!`)
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    let success = rule.parser(text, start, mynodes)
    return if success == -1: 1 else: -1
  result = newRule[N, T](parser, rule.kind)

proc `*`*[N, T](repetitions: int, rule: Rule[N, T]): Rule[N, T] =
  ## Matches `rule`, `repetition` times
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    var total = 0
    for i in 0..<repetitions:
      let success = rule.parser(text, start+total, mynodes)
      if success == -1:
        return success
      else:
        total += success
    nodes.add(mynodes)
    return total
  result = newRule[N, T](parser, rule.kind)

proc `&`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Matches, so long as `rule` would match at this point (i.e. zero-width lookahead)
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    let success = rule.parser(text, start, mynodes)
    return if success != -1: 0 else: -1
  result = newRule[N, T](parser, rule.kind)

proc `!`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  ## Matches, so long as `rule` would *not* match at this point (i.e. negative zero-width lookahead)
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    let failure = rule.parser(text, start, mynodes)
    return if failure == -1: 0 else: -1
  result = newRule[N, T](parser, rule.kind)

proc `/`*[N, T](rule: Rule[N, T]): Rule[N, T] =
  let parser = proc (text: T, start: int, nodes: var seq[Node[N, T]]): int =
    var mynodes = newSeq[Node[N, T]]()
    var length = 0
    var success = rule.parser(text, start+length, mynodes)
    while success == -1 and start+length < len(text):
      length += 1
      success = rule.parser(text, start+length, mynodes)
    if start+length >= len(text):
      result = -1
    else:
      nodes.add(newNode(start, length, rule.kind, text))
      nodes.add(mynodes)
      result = length + success
  result = newRule[N, T](parser, rule.kind)

proc `->`*(rule: Rule, production: Rule) =
  if isnil(production.parser):
    raise newException(ValueError, "Right hand side of -> is nil - has the rule been defined yet?")
  rule.parser = production.parser

template grammar*[K: enum](Kind, Text, Symbol: typedesc; default: K, code: untyped): void =
    # Template type annotations
    # Kind - the type of node you want to test for
    # Text - the text you are parsing: either a string or a seq[Node[S]]
    # P(attern) - either a string to look for in the text of a seq[S] to look for inside the text
    # S(ymbol) - either a char within the text or the kind of the nodes in the seq[Node[S]]

    proc newRule(): Rule[Kind, Text] {.inject used.} = newRule[Kind, Text](default)
    proc chartest(testfunc: proc(c: Symbol): bool): Rule[Kind, Text] {.inject used.} = chartest[Kind, Text, Symbol](testfunc, default)
    proc literal[P](pattern: P, kind: K): Rule[Kind, Text] {.inject used.} = literal[Kind, Text, P](pattern, kind)
    proc literal[P](pattern: P): Rule[Kind, Text] {.inject used.} = literal[Kind, Text, P](pattern, default)
    
    when Text is string:
      proc iliteral(pattern: string): Rule[Kind, Text] {.inject used.} = iliteral[Kind](pattern, default)
      proc any(chars: string): Rule[Kind, Text] {.inject used.} = any[Kind](chars, default)
      proc token(pattern: string): Rule[Kind, Text] {.inject used.} = token(pattern, default)
      proc fail(message: string): Rule[Kind, Text] {.inject used.} = fail[Kind, Text](message, default)
      proc error(message: string): Rule[Kind, Text] {.inject used.} = error[Kind, Text](message, default)
      proc dump(message: string): Rule[Kind, Text] {.inject used.} = dump[Kind, Text](message, default)
      let alpha {.inject used.} = chartest[Kind, Text, Symbol](isAlphaAscii, default)
      let alphanumeric {.inject used.}= chartest[Kind, Text, Symbol](isAlphaNumeric, default)
      let digit {.inject used.} = chartest[Kind, Text, Symbol](isDigit, default)
      let lower {.inject used.} = chartest[Kind, Text, Symbol](isLowerAscii, default)
      let upper {.inject used.} = chartest[Kind, Text, Symbol](isUpperAscii, default)
      let isspace = proc (x: char): bool = x.isSpaceAscii and not (x in NewLines)
      let space {.inject used.} = chartest[Kind, Text, Symbol](isspace, default)
      let isnewline = proc (x: char): bool = x in NewLines
      let newline {.inject used.} = chartest[Kind, Text, Symbol](isnewline, default)
      let alphas {.inject used.} = combine(+alpha, default)
      let alphanumerics {.inject used.} = combine(+alphanumeric, default)
      let digits {.inject used.} = combine(+digit, default)
      let lowers {.inject used.} = combine(+lower, default)
      let uppers {.inject used.} = combine(+upper, default)
      let spaces {.inject used.} = combine(+space, default)
      let newlines {.inject used.} = combine(+newline, default)
      let eof {.inject used.} = eof[Kind, Text](default)
    else:
      proc any[S](symbols: seq[S]|set[S]): Rule[Kind, Text] {.inject used.} = any[Kind, Text, S](symbols, default)
      proc any[S](symbols: seq[S]|set[S], kind: Kind): Rule[Kind, Text] {.inject used.} = any[Kind, Text, S](symbols, kind)
    
    proc combine(rule: Rule[Kind, Text]): Rule[Kind, Text] {.inject used.} = combine[Kind, Text](rule, default)
    
    code
  
template grammar*[K](Kind: typedesc; default: K, code: untyped): void =
  grammar(Kind, string, char, default, code)