import pegs
import unittest

import ../src/grammar

const JSON = staticRead("smalltest.json")

suite "Grammar tests":
  test "Testing simple patterns":
    block:
      type DummyKind = enum dkDefault
      grammar(DummyKind, string, char, dkDefault):
        let rule = token("h[a]+m") + ignore(token(r"\s+")) + (literal("eggs") / literal("beans"))
        var text = "ham beans"
        let tokentest = rule.parse(text)
        require(len(tokentest)>0)

        var recursive = newRule()
        recursive -> (literal("(") + recursive + literal(")")) / token(r"\d+")
        for test in ["57", "(25)", "((25))"]:
          let recursivetest = recursive.parse(test)
          require(len(recursivetest)>0)
        for test in ["spam"]:
          let parsed = recursive.parse(test)
          require(len(parsed)==0)

        let repeated = +literal("spam") + ?literal("ham") + *literal("salami")
        
        for test in ["spam", "spamspamspam" , "spamham", "spamsalami", "spamsalamisalami"]:
          let parsed = repeated.parse(test)
          require(len(parsed)>0)
        for test in ["ham"]:
          let parsed = repeated.parse(test)
          require(len(parsed)==0)
  
  test "Test failure":
    block:
      type DummyKind = enum dkDefault
      grammar(DummyKind, string, char, dkDefault):
        let rule = literal("wrong") + ((literal("wrong") + error("Two wrongs!")) / ?literal("right"))
        for test in ["wrong", "wrongright"]:
          require(len(rule.parse(test))>0)
        expect ValueError:
          let parsed = rule.parse("wrongwrong")
          echo parsed.render
  
  test "Test argparse-style patterns":
    # The peg we're trying to emulate       
    let optionPeg = peg"""
      option <- (space+)? ({short / long}({[ :=] value})? ' ')+  space+ help
      help <- {@} default?
      default <- {'[default: '} {(\letter / [0-9-_.])+} {']'}
      short <- '-' (\letter / \d)
      long <- '--' name
      name <- (\letter / [0-9-_.])+
      value <- '<' name '>' / (\upper)+
      space <- (!\n \s)+
    """

    type
      ParseKind = enum
        pkDefault, pkHelp, pkDefaultValue

    block:
      grammar(ParseKind, string, char, pkDefault):
        var option = newRule()
        var help = newRule()
        var short = newRule()
        var long = newRule()
        var name = newRule()
        var value = newRule()
        var defaultValue = newRule()

        option -> ignore(*space) + short / long + ?(ignore(any(" :=")) + value) + (ignore(2 * space) / fail("There must be two spaces between options and the start of the help text")) + help
        help -> /(ignore(*space + literal("[default: ")) + defaultvalue + ignore(literal("]"))) / token(".*")
        short -> ignore(literal("-")) + alphanumeric
        long -> ignore(literal("--")) + name
        name -> combine(+alphanumeric)
        value -> (ignore(literal("<")) + name + ignore(literal(">"))) / uppers
        defaultValue -> alphanumerics

      for test in  ["  --foo  Use a foo",
                    "-h  Print help",
                    "--baz=BAZ  Value of baz",
                    "--biff  Value of biff [default: 17]"]:
        check(test =~ optionPeg)
        check(len(option.parse(test))>0)
  test "Test simple JSON parser":
    block:
      type JsonKind = enum jkDefault, jkLeftCurly, jkRightCurly, jkRightSquare,
                            jkLeftSquare, jkString, jkComma, jkColon, jkBoolean,
                            jkDecimal, jkInteger
      grammar(JsonKind, string, char, jkDefault):
        var document = newRule()
        var linecomment = newRule()
        var blockcomment = newRule()
        var obj = newRule()
        var key = newRule()
        var keysep = newRule()
        var objsep = newRule()
        var arr = newRule()
        var arrsep = newRule()
        var val = newRule()
        var str = newRule()
        var innerstr = newRule()
        var truth = newRule()
        var number = newRule()
        var null = newRule()
        var s = newRule()
        var integer = newRule()
        var decimal = newRule()
        var exponent = newRule()

        document -> *(obj / arr / val) + s
        linecomment -> ignore(literal("//")) + /ignore(literal("\n"))
        blockcomment -> ignore(literal("/*")) + /ignore(literal("*/"))
        obj -> s + literal('{') + s + ?((str + s + keysep + s + val) + *(s + objsep + s + str + s + keysep + s + val) + s) + literal('}')
        arr -> s + literal('[') + s + ?(val + *(s + arrsep + s + val) + s) + literal(']')
        key -> str / fail("Object keys must be strings")
        keysep -> literal(':', jkColon) / fail("Object keys must be separated from their values by :")
        objsep -> literal(',', jkComma) / &(s + literal('}')) / fail("Contents of objects must be separated by ,")
        arrsep -> literal(',', jkComma) / &(s + literal(']')) / fail("Contents of arrays must be separated by ,")
        val -> s + (str / number / obj / arr / truth / null)
        number -> decimal / integer
        decimal -> combine(combine(?literal('-') + digits + ?(literal('.') + +digits)) + ?exponent, jkDecimal)
        exponent -> ignore(any("Ee")) + combine(?any("+-") + +digits)
        integer -> combine(?literal('-') + digits, jkInteger)
        str -> ignore(literal('"')) + combine(innerstr, jkString) + ignore(literal('"'))
        innerstr -> *( (literal('\\') + (alpha / any("\"\\"))) / ^literal('"') )
        # innerstr -> token(r"""(([\\].)|([^"]))*+""")
        truth -> literal("true", jkBoolean) / literal("false", jkBoolean)
        null -> literal("null")
        s -> *(ignore(spaces) / ignore(newlines) / (linecomment / blockcomment))

        discard(document.parse(r""" {"fooz" : "baz"} """))
        discard(document.parse(r""" "foo" """))
        discard(document.parse(r""" [1, 2, 3 , 4, 5] """))
        discard(document.parse(r""" [1, 2, 3, 4, 5] """))
        discard(document.parse(r""" "baz \\ \" "  """))
        discard(document.parse(r""" {"key": "value"}  """))
        # discard(document.parse(r" {"foo" : [1,2.0, 3.0e5], "baz" : true, "bif" : null , "a" : [], "b": {}} """))

        check(len(document.parse(JSON))>0)
